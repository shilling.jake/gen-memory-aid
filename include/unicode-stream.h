#ifndef UNCODE_STREAM_H
#define UNCODE_STREAM_H

#include <stdint.h>

extern const int EENCODING;

/* Type to wrap around a POSIX file descriptor and perform
   io operations on UTF-8 characters */
typedef struct UnicodeStream UnicodeStream;

/* Open a UnicodeStream. Works like fopen */
UnicodeStream *unicode_stream_fopen(const char * restrict, const char * restrict);

/* Gets the next character from stream. */
int unicode_stream_fgetc(UnicodeStream *);

int unicode_stream_fclose(UnicodeStream *);
int unicode_stream_feof(UnicodeStream *);
int unicode_stream_ferror(UnicodeStream *);

#endif /* UNCODE_STREAM_H */
