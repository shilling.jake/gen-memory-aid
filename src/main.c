/*
 * This file is part of gen-memory-aid.
 *
 * gen-memory-aid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gen-memory-aid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gen-memory-aid.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include <config.h>

/* It seems that gcc does not include the POSIX getopt prototypes when C
   extensions are disabled. */
int getopt(int, char *const[], const char *);
extern char *optarg;
extern int optind, opterr, optopt;

int main(int argc, char *argv[]) {
  /* Parse command line options */
  int c = 0;

  int vflag = 0;
  int hflag = 0;
  int nerrs = 0;
  char *ofile = NULL;
  char *ifile = NULL;

  while (optind < argc) {
    while ((c = getopt(argc, argv, ":o:hv")) != -1) {
      switch (c) {
      case 'v':
        vflag++;
        break;

      case 'h':
        hflag++;
        break;

      case 'o':
        if (ofile) {
          fputs("Cannot specify more than one output file!\n", stderr);
          nerrs++;
	  hflag++;
        } else {
          ofile = optarg;
        }
        break;

      case ':':
        fprintf(stderr, "Missing filename with option -%c\n", optopt);
        hflag++;
        nerrs++;
        break;

      case '?':
        fprintf(stderr, "Unrecognized option -%c\n", optopt);
        hflag++;
        nerrs++;
        break;
      }
    }

    if (optind < argc) {
      if (ifile) {
        fputs("Connot specify more than one input file!\n", stderr);
        nerrs++;
	hflag++;
      } else {
        ifile = argv[optind];
      }

      optind++;
    }
  }

  /* TAKE ACTION APPROPIRATE FOR THE FOUND FLAGS */
  /* print version */
  if (vflag) {
    printf("%s, version %d.%d\n", PACKAGE_NAME, MAJOR_VERSION, MINOR_VERSION);
    puts("Copyright (c) 2019 Jake Shilling");
  }

  /* print usage */
  if (hflag) {
    puts("Usage:");
    printf("  %s command_options\n", PACKAGE_NAME);
    printf("  %s [output_options] [<INTPUT_FILE>]\n\n", PACKAGE_NAME);
    puts("command_options:");
    puts("  -v               show version string");
    puts("  -h               show this message");
    puts("output_options:");
    puts("  -o <FILE>        specify output file; defaults to stdout");
    puts("  INPUT_FILE       specify input file; defaults to stdin");
  }

  /* process input file */
  if (!vflag && !hflag) {
    if (ofile) {
      printf("output to \'%s\'\n", ofile);
    } else {
      puts("output to stdout");
    }

    if (ifile) {
      printf("input from \'%s\'\n", ifile);
    } else {
      puts("input from stdin");
    }
  }

  /* All done */
  return nerrs == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
