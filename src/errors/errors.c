#include <stdio.h>

/* POSIX errno values are all positive integers and EOF is defined as
   some negative integer (usually -1). Our errors will us the rest of
   the negative integer space by counting down from EOF. */

const int EENCODING = (EOF - 1);
