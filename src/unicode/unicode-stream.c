/*
 * This file is part of gen-memory-aid.
 *
 * gen-memory-aid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gen-memory-aid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gen-memory-aid.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unicode-stream.h>

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUF_SIZE (512)

struct UnicodeStream {
  /* POSIX file descriptor */
  int fd;

  /* EOF indicator */
  int eof;
  /* error indicator */
  int err;

  int (*fgetc)(UnicodeStream *);

  /* current location in buffer */
  int index;
  /* number of bytes read into buffer */
  ssize_t bytes_read;
  /* byte array buffer */
  uint8_t buffer[BUF_SIZE];
};

/* Takes a mode string suitable for fopen and returns corresponding
   flags for open. If the mode string is invalid, returns -1.

   'r' or 'rb' -> O_RDONLY
   'w' or 'wb' -> O_WRONLY|O_CREAT|O_TRUNC
   'a' or 'ab' -> O_WRONLY|P_CREAT|O_APPEND
   'r+', 'rb+' or 'r+b' -> O_RDWR
   'w+', 'wb+' or 'w+b' -> O_RDWR|O_CREAT|O_TRUNC
   'a+', 'ab+' or 'a+b' -> O_RDWR|O_CREAT|O_APPEND */
static int __parse_mode_string(const char *restrict mode) {
  /* internally used flags */
  static const int R_FLAG = (1 << 0);
  static const int W_FLAG = (1 << 1);
  static const int A_FLAG = (1 << 2);
  static const int P_FLAG = (1 << 3);
  static const int B_FLAG = (1 << 4);

  /* null check */
  if (NULL == mode) {
    goto invalid_mode_string;
  }

  /* Compile mode string to a collection of internal flags,
     stored here. */
  int flags = 0;

  /* First char must be 'r', 'w', or 'a' */
  switch (mode[0]) {
  case 'r':
    flags |= R_FLAG;
    break;
  case 'w':
    flags |= W_FLAG;
    break;
  case 'a':
    flags |= A_FLAG;
    break;
  default:
    goto invalid_mode_string;
  }

  /* Second and third char must be 'b', '+' or '\0' */
  int i = 1;
  while (i < 3) {
    switch (mode[i]) {
    case 'b':
      /* only one b permitted */
      if (flags & B_FLAG)
        goto invalid_mode_string;
      else
        flags |= B_FLAG;
      break;
    case '+':
      /* only one + permitted */
      if (flags & P_FLAG)
        goto invalid_mode_string;
      else
        flags |= P_FLAG;
      break;
    case '\0':
      goto end_of_string;
    default:
      goto invalid_mode_string;
    }
  }

  /* Fourth char must be '\0' */
  if ('\0' != mode[3])
    goto invalid_mode_string;

end_of_string:
  /* Convert internal flags to open flags */
  if (flags & (R_FLAG | P_FLAG))
    return O_RDWR;
  else if (flags & (W_FLAG | P_FLAG))
    return O_RDWR | O_CREAT | O_TRUNC;
  else if (flags & (A_FLAG | P_FLAG))
    return O_RDWR | O_CREAT | O_APPEND;
  else if (flags & R_FLAG)
    return O_RDONLY;
  else if (flags & W_FLAG)
    return O_WRONLY | O_CREAT | O_TRUNC;
  else if (flags & A_FLAG)
    return O_WRONLY | O_CREAT | O_APPEND;

  /* it should be imposible to get here with an invalid
     collection of flags, but if that happens we just fall
     through to the invalid_mode_string label anyway. */

invalid_mode_string:
  return -1;
}

/* Try to read from the file descriptor into the buffer. Set
   err or eof if appropriate. Return 0 on success and -1 on
   failure or eof. */
static int __fill_buffer(UnicodeStream *stream) {
  /* Reset index and bytes_read */
  stream->index = 0;
  /* Do the file read */
  stream->bytes_read = read(stream->fd, stream->buffer, BUF_SIZE);

  /* Check for errors and eof */
  if (-1 == stream->bytes_read) {
    /* save errno */
    stream->err = errno;
    return errno;
  } else if (0 == stream->bytes_read) {
    stream->eof = EOF;
    return EOF;
  } else {
    return 0;
  }
}

/* Return the next byte cast up to an int or EOF if we can't get the
   next byte for any reason. */
static int __next_byte(UnicodeStream *stream) {
  /* Check for NULL */
  if (stream) {
    /* Check whether we need to refill the stream */
    if (stream->index >= stream->bytes_read) {
      /* Check for errors on read */
      if (__fill_buffer(stream)) {
        /* Nothing was read */
        return EOF;
      }
    }

    /* return next byte */
    return stream->buffer[stream->index++];
  }

  /* Cannot read from NULL */
  return EOF;
}

/* Implementation of unicode_stream_fgetc which assumes UTF-8 encoding */
static int __utf8_unicode_stream_fgetc(UnicodeStream *stream) {
  /* __next_byte checks for NULL */
  int ret = __next_byte(stream);

  if (EOF != ret) {
    int leading_bits = 0;
    for (int i = 0; (i < 8) && (ret & (1 << (7 - i))); i++) {
      /* Count and turn off bit */
      leading_bits++;
      ret &= ~(1 << (7 - i));
    }

    /* leading bits count the total number of bytes */
    for (int i = 1; i < leading_bits; i++) {
      int next_byte = __next_byte(stream);
      if (EOF == next_byte) {
        /* Check if EOF was caused by an error */
        if (unicode_stream_ferror(stream)) {
          goto read_error;
        } else {
          /* File ended in the middle of a character */
          goto encoding_error;
        }
      }

      /* Check valid byte */
      if (0x80 != (next_byte & 0xc0)) {
        goto encoding_error;
      } else {
        /* turn off validation bits */
        next_byte &= ~(0x80);
      }

      /* Add meaningful bits to ret */
      ret <<= 6;
      ret |= next_byte;
    }
  }

  return ret;
encoding_error:
  stream->err = EENCODING;
read_error:
  errno = stream->err;
  return EOF;
}

/* Works like fopen, but opens a UnicodeStream. */
UnicodeStream *unicode_stream_fopen(const char *restrict path,
                                    const char *restrict mode) {
  /* Try to figure out the flags */
  int flags;
  if (-1 == (flags = __parse_mode_string(mode))) {
    /* Invalid mode string */
    errno = EINVAL;
    return NULL;
  }

  /* Try to allocate some memory */
  UnicodeStream *ret = (UnicodeStream *)malloc(sizeof(UnicodeStream));
  if (!ret) {
    /* errno already set */
    return NULL;
  }

  /* Try to open the file */
  int fd = open(path, flags);
  if (-1 == fd) {
    /* errno already set */
    free(ret);
    ret = NULL;
  }

  /* Made it past everything that might fail. Initialize the
     UnicodeStream object */
  ret->fd = fd;
  ret->eof = 0;
  ret->err = 0;
  ret->index = 0;
  ret->bytes_read = 0;
  ret->fgetc = __utf8_unicode_stream_fgetc;

  /* Fill the buffer so we can check for a BOM */
  if (!__fill_buffer(ret)) {
    /* The file might start with a BOM, in which case the first call
       to read from the stream whould return the bytes after the BOM.
       While we're at it, also check to see if this is not a UTF-8
       file. */

    /* Check for 2-byte BOM */
    if (2 <= ret->bytes_read) {
      uint32_t bom = *((uint16_t *)ret->buffer);
      if ((0xfeff == bom) || (0xfffe == bom)) {
        /* File is UTF-16 */
        ret->err = EENCODING;
      } else if (3 <= ret->bytes_read) {
        if ((0xef == ret->buffer[0]) && (0xbb == ret->buffer[1]) &&
            (0xbf == ret->buffer[2])) {
          /* UTF-8 BOM found */
          ret->index = 3;
        } else if (4 <= ret->bytes_read) {
          bom = *((uint32_t *)ret->buffer);
          if ((0x0000feff == bom) || (0xfffe0000 == bom)) {
            /* UTF-32 BOM found */
            ret->err = EENCODING;
          }
        }
      }
    }
  }

  return ret;
}

int unicode_stream_fclose(UnicodeStream *stream) {
  if (stream) {
    if (close(stream->fd)) {
      /* errno is already set */
      return -1;
    }

    free(stream);
  }

  return 0;
}

int unicode_stream_fgetc(UnicodeStream *stream) {
  if (stream)
    return stream->fgetc(stream);
  else
    return EOF;
}

int unicode_stream_feof(UnicodeStream *stream) {
  if (stream) {
    return stream->eof;
  } else {
    return 0;
  }
}

int unicode_stream_ferror(UnicodeStream *stream) {
  if (stream) {
    return stream->err;
  } else {
    return 0;
  }
}
