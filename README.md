# Memory Aid Generator

Memory is a learned skill that comes with practice. As a classroom
teacher I watched coutless students try to memorize things, but fail
simply because they didn't know exactly *what* to practice. Mostly,
they would try to repeat *saying* some list of words or a passage
repeatedly and get nowhere--because this only let the practice *saying*,
when they really needed to practice *remembering*.

To make it a little easier to avoid the trap of trying to memorize a
text by simply looking at it, I like to make memory aids when provide
only the first letter of each word. This is a little easier than
trying to remember without any kind of help, but still gives an
opportunity to practice the act of *recalling*.

This is a tool to help generate these memory aids. It will take a text
file and output a copy with each word replaced with dashes except for
the first word. So:
```
Arma virumque cano, Troiae qui primus ab oris
```
will become
```
A--- v------ c---, T----- q-- p----- a- o---
```
