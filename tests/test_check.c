#include <stdio.h>
#include <check.h>

START_TEST(test_func)
{
  ck_assert(1);
}
END_TEST

int main (int argc, char *argv[]) {
  int number_failed;
  Suite *suite = suite_create("Test Suite");
  TCase *tcase = tcase_create("case");

  tcase_add_test(tcase, test_func);
  suite_add_tcase(suite, tcase);

  SRunner *runner = srunner_create(suite);
  srunner_run_all(runner, CK_NORMAL);
  number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);
  return number_failed;
}
