#include <check.h>
#include <stdint.h>
#include <stdio.h>

#include <unicode-stream.h>

#define UTF8_NO_BOM "utf8_no_bom.txt"
#define UTF8_WITH_BOM "utf8_with_bom.txt"

static const uint8_t utf8_bom[] = {0xef, 0xbb, 0xbf};

/* Arma virumque canō, Troiae quī primus ab ōrīs */
static const uint32_t aeneid1_ucs4[] = {
    0x41, 0x72, 0x6D, 0x61, 0x20, 0x76,  0x69, 0x72,  0x75, 0x6D, 0x71, 0x75,
    0x65, 0x20, 0x63, 0x61, 0x6E, 0x14D, 0x2C, 0x20,  0x54, 0x72, 0x6F, 0x69,
    0x61, 0x65, 0x20, 0x71, 0x75, 0x12B, 0x20, 0x70,  0x72, 0x69, 0x6D, 0x75,
    0x73, 0x20, 0x61, 0x62, 0x20, 0x14D, 0x72, 0x12B, 0x73, 0xA};

static const uint8_t aeneid1_utf8[] = {
    0x41, 0x72, 0x6d, 0x61, 0x20, 0x76, 0x69, 0x72, 0x75, 0x6d,
    0x71, 0x75, 0x65, 0x20, 0x63, 0x61, 0x6e, 0xc5, 0x8d, 0x2c,
    0x20, 0x54, 0x72, 0x6f, 0x69, 0x61, 0x65, 0x20, 0x71, 0x75,
    0xc4, 0xab, 0x20, 0x70, 0x72, 0x69, 0x6d, 0x75, 0x73, 0x20,
    0x61, 0x62, 0x20, 0xc5, 0x8d, 0x72, 0xc4, 0xab, 0x73, 0x0a};

static void __test_file(const char *path) {
  /* Open file as UnicodeStream */
  UnicodeStream *stream = unicode_stream_fopen(path, "r");
  ck_assert_ptr_nonnull(stream);

  /* Check chars */
  for (int i = 0; i < (sizeof(aeneid1_ucs4) / sizeof(uint32_t)); i++) {
    ck_assert_int_eq(aeneid1_ucs4[i], unicode_stream_fgetc(stream));
  }

  /* Check EOF */
  ck_assert_int_eq(EOF, unicode_stream_fgetc(stream));
  ck_assert(unicode_stream_feof(stream));

  /* Close stream */
  ck_assert_int_eq(0, unicode_stream_fclose(stream));
}

START_TEST(test_read_utf8_from_file_without_bom) { __test_file(UTF8_NO_BOM); }
END_TEST

START_TEST(test_read_utf8_from_file_with_bom) { __test_file(UTF8_WITH_BOM); }
END_TEST

int main(int argc, char *argv[]) {
  /* Write files need for tests */
  /* utf8 file without a bom */
  FILE *file = fopen(UTF8_NO_BOM, "w");
  fwrite(aeneid1_utf8, 1, sizeof(aeneid1_utf8), file);
  fclose(file);
  /* utf8 file with a bom */
  file = fopen(UTF8_WITH_BOM, "w");
  fwrite(utf8_bom, 1, sizeof(utf8_bom), file);
  fwrite(aeneid1_utf8, 1, sizeof(aeneid1_utf8), file);
  fclose(file);

  int number_failed;
  Suite *suite = suite_create("libunicode");
  TCase *tcase = tcase_create("read from file");

  tcase_add_test(tcase, test_read_utf8_from_file_without_bom);
  tcase_add_test(tcase, test_read_utf8_from_file_with_bom);
  suite_add_tcase(suite, tcase);

  SRunner *runner = srunner_create(suite);
  srunner_set_fork_status(runner, CK_NOFORK);
  srunner_run_all(runner, CK_VERBOSE);
  number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);

  /* Delete test files */
  remove(UTF8_NO_BOM);
  remove(UTF8_WITH_BOM);

  return number_failed;
}
